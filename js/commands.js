let commands = {
  login: {
    help: function (args) {
        return '<br><strong>NAME</strong><br>' +
            '&nbsp;&nbsp;&nbsp;&nbsp;login<br><br>' +
            '<strong>SYNOPSIS</strong><br>' +
            '&nbsp;&nbsp;&nbsp;&nbsp;login [USER]<br><br>' +
            '<strong>DESCRIPTION</strong><br>' +
            '&nbsp;&nbsp;&nbsp;&nbsp;Login using the given username.<br><br>';
    },
    callback: function (args) {
        let user = this.utils.arrayOfObjectsHasKeyValue(lightdm.users, 'name', args[0]);
        if (!user) {
            this.stderr(`bash: no such user: ${args[0]}`);
            return false;
        }
        if (lightdm.in_authentication) {
            lightdm.cancel_authentication();
        }
        this.session = user.session !== null && user.session === this.session ? user.session : this.session;
        lightdm.start_authentication(user.name);
        return true;
    },
    password: function (password, response) {
        if (lightdm.in_authentication) {
            setTimeout(function () {
                lightdm.respond(password);
            }, 200);
            return null;
        }
        return `call login [user]<br>`;
    }
  },
}